package com.test.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Servlet implementation class FirstServlet
 */
/**
 * @author preetham
 *
 */
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private final List<String> emails = new ArrayList<>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		emails.clear();

		System.out.println("FirstServlet \"Service\" method(inherited) called");
		System.out.println("FirstServlet \"DoGet\" method called");
		
		storeInSessionAndRespond(request, response);
		response.setStatus(302);
		response.addHeader("Location", "https://google.com");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Object name = session.getAttribute("name");
		request.authenticate()

		System.out.println("FirstServlet \"Service\" method(inherited) called");
        System.out.println("FirstServlet \"DoPost\" method called");

		String email = request.getParameter("email");
		emails.add(email);
		PrintWriter pw = response.getWriter();
		getClass().getClassLoader().getResource("google.txt");
		Stream<String> lines = null;
		try {
			lines = Files.lines(Paths.get(getClass().getClassLoader().getResource("google.txt").toURI()), StandardCharsets.UTF_8);
			lines.forEach(line -> pw.write(line));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		//storeInSessionAndRespond(request, response);

	}
	
	private void storeInSessionAndRespond(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		String uname = request.getParameter("uname");
		String emailId = request.getParameter("email");
		System.out.println("Username from jsp page is "+ uname + " and email id is "+ emailId);
		//Create a session
		HttpSession session = request.getSession(true);
		if(session!=null)
		{
			//store the attributes
			session.setAttribute("uname", uname);
			session.setAttribute("emailId", emailId);
			System.out.println("Username and email id is stored in the session");
		}

		out.write("<html><body> Number of emails: " + emails.size() + "</body></html>");
	}



}
