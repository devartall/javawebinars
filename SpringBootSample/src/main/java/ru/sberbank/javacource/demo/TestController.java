package ru.sberbank.javacource.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private BookService bookService;

    @GetMapping("/book")
    public ResponseEntity<Book> getBook() {
        final Book book = bookService.getBook();
        return ResponseEntity.ok(book);
    }
}
