### Домашнее задание по уроку 1

#### Теория

- [Тест](https://forms.gle/nJT4ZJFgGrV6S4CP8)
- [Горячие клавишы IDEA](https://resources.jetbrains.com/storage/products/intellij-idea/docs/IntelliJIDEA_ReferenceCard.pdf)
- [Статья по Maven](http://java-online.ru/maven-pom.xhtml)

#### Практика

- Добавить Java класс `GreetingService` с методом
  `public static String sayHello()`, который будет возвращать строку  “Hello”
- Заменить использование литерала “Hello” на вызов метода, в `MyFirstJavaProgram`
- Собрать jar-архив из получившихся 3-х классов и описать главный
класс в манифесте
- Желательно попробовать из консоли и из IntelliJ IDEA