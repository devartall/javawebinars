package lesson10.foodmachine.oop;

import lesson10.foodmachine.oop.factory.FitnessFoodMachineFactory;
import lesson10.foodmachine.oop.factory.FoodMachineFactory;
import lesson10.foodmachine.oop.machine.FoodMachine;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-10
 */
public class Driver {

    public static void main(String[] args) {
        FoodMachineFactory fitnessFactory = new FitnessFoodMachineFactory();
        FoodMachine foodMachine = fitnessFactory.createFoodMachine();


    }
}
