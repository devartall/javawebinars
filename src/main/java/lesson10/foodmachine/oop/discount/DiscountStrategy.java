package lesson10.foodmachine.oop.discount;

import lesson10.foodmachine.oop.machine.Cell;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-14
 */
public interface DiscountStrategy {

    int make(Cell cell);
}
