package lesson10.foodmachine.oop.factory.product;

import lesson10.foodmachine.oop.product.bar.Bar;
import lesson10.foodmachine.oop.product.bar.ProteinBar;
import lesson10.foodmachine.oop.product.dessert.Cheesecake;
import lesson10.foodmachine.oop.product.dessert.Dessert;
import lesson10.foodmachine.oop.product.fruit.Banana;
import lesson10.foodmachine.oop.product.fruit.Fruit;
import lesson10.foodmachine.oop.product.meat.ChickenBreast;
import lesson10.foodmachine.oop.product.meat.Meat;
import lesson10.foodmachine.oop.product.pasta.Pasta;
import lesson10.foodmachine.oop.product.pasta.PastaWithVegetables;
import lesson10.foodmachine.oop.product.porridge.Porridge;
import lesson10.foodmachine.oop.product.porridge.RicePorridge;
import lesson10.foodmachine.oop.product.salad.FreshVegetableSalad;
import lesson10.foodmachine.oop.product.salad.Salad;
import lesson10.foodmachine.oop.product.shake.ProteinShake;
import lesson10.foodmachine.oop.product.shake.Shake;
import lesson10.foodmachine.oop.product.water.StillWater;
import lesson10.foodmachine.oop.product.water.Water;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class FitnessProductFactory implements ProductFactory {


    /*---------------------------SINGLETON---------------------------*/


    private static FitnessProductFactory factory = new FitnessProductFactory();

    private FitnessProductFactory() {}

    public static FitnessProductFactory getInstance() {
        return factory;
    }


    /*---------------------------ABSTRACT FACTORY---------------------------*/


    @Override
    public Water createWater() {
        return new StillWater();
    }

    @Override
    public Bar createBar() {
        return new ProteinBar();
    }

    @Override
    public Salad createSalad() {
        return new FreshVegetableSalad();
    }

    @Override
    public Fruit createFruit() {
        return new Banana();
    }

    @Override
    public Dessert createDessert() {
        return new Cheesecake();
    }

    @Override
    public Porridge createPorridge() {
        return new RicePorridge();
    }

    @Override
    public Meat createMeat() {
        return new ChickenBreast();
    }

    @Override
    public Pasta createPasta() {
        return new PastaWithVegetables();
    }

    @Override
    public Shake createShake() {
        return new ProteinShake();
    }
}
