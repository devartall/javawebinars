package lesson10.foodmachine.oop.factory.product;

import lesson10.foodmachine.oop.product.bar.Bar;
import lesson10.foodmachine.oop.product.bar.ChocolateBar;
import lesson10.foodmachine.oop.product.dessert.Dessert;
import lesson10.foodmachine.oop.product.dessert.Doughnut;
import lesson10.foodmachine.oop.product.fruit.Fruit;
import lesson10.foodmachine.oop.product.fruit.StrawberryWithCream;
import lesson10.foodmachine.oop.product.meat.Cutlet;
import lesson10.foodmachine.oop.product.meat.Meat;
import lesson10.foodmachine.oop.product.pasta.Carbonara;
import lesson10.foodmachine.oop.product.pasta.Pasta;
import lesson10.foodmachine.oop.product.porridge.Porridge;
import lesson10.foodmachine.oop.product.porridge.PorridgeWithJam;
import lesson10.foodmachine.oop.product.salad.DressedHerring;
import lesson10.foodmachine.oop.product.salad.Salad;
import lesson10.foodmachine.oop.product.shake.MilkShake;
import lesson10.foodmachine.oop.product.shake.Shake;
import lesson10.foodmachine.oop.product.water.SodaWater;
import lesson10.foodmachine.oop.product.water.Water;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public class GeneralProductFactory implements ProductFactory {


    /*---------------------------SINGLETON---------------------------*/


    private static GeneralProductFactory factory = new GeneralProductFactory();

    private GeneralProductFactory() {}

    public static GeneralProductFactory getInstance() {
        return factory;
    }


    /*---------------------------ABSTRACT FACTORY---------------------------*/


    @Override
    public Water createWater() {
        return new SodaWater();
    }

    @Override
    public Bar createBar() {
        return new ChocolateBar();
    }

    @Override
    public Salad createSalad() {
        return new DressedHerring();
    }

    @Override
    public Fruit createFruit() {
        return new StrawberryWithCream();
    }

    @Override
    public Dessert createDessert() {
        return new Doughnut();
    }

    @Override
    public Porridge createPorridge() {
        return new PorridgeWithJam();
    }

    @Override
    public Meat createMeat() {
        return new Cutlet();
    }

    @Override
    public Pasta createPasta() {
        return new Carbonara();
    }

    @Override
    public Shake createShake() {
        return new MilkShake();
    }
}
