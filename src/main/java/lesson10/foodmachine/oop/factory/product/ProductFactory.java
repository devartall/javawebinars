package lesson10.foodmachine.oop.factory.product;

import lesson10.foodmachine.oop.product.bar.Bar;
import lesson10.foodmachine.oop.product.dessert.Dessert;
import lesson10.foodmachine.oop.product.fruit.Fruit;
import lesson10.foodmachine.oop.product.meat.Meat;
import lesson10.foodmachine.oop.product.pasta.Pasta;
import lesson10.foodmachine.oop.product.porridge.Porridge;
import lesson10.foodmachine.oop.product.salad.Salad;
import lesson10.foodmachine.oop.product.shake.Shake;
import lesson10.foodmachine.oop.product.water.Water;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-01-15
 */
public interface ProductFactory {


    /*---------------------------ABSTRACT FACTORY---------------------------*/


    Water createWater();

    Bar createBar();

    Salad createSalad();

    Fruit createFruit();

    Dessert createDessert();

    Porridge createPorridge();

    Meat createMeat();

    Pasta createPasta();

    Shake createShake();
}
