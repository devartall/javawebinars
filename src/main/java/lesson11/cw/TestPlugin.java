package lesson11.cw;

import lesson11.cw.annotations.Plugin;

@Plugin("test")
public class TestPlugin implements IPlugin {

    public static String value = "";
    private final String password = "12345";
    private int[] newsIds;

    class InnerClass {
        int iValue;
        String sValue;
    }

    @Override
    public void init() {
        String name = getClass().getAnnotation(Plugin.class).value();
        System.out.println(name + " instantiated");
        System.out.println(getClass().getCanonicalName() + " instantiated");
        System.out.println(getClass().getSimpleName() + " instantiated");

        System.out.println();

        newsIds = new int[10];
        System.out.println(newsIds.getClass().getName() + " instantiated");
        System.out.println(newsIds.getClass().getCanonicalName() + " instantiated");
        System.out.println(newsIds.getClass().getSimpleName() + " instantiated");

        InnerClass innerClass = new InnerClass();
        System.out.println();
        System.out.println(innerClass.getClass().getName());
        System.out.println(innerClass.getClass().getCanonicalName());
        System.out.println(innerClass.getClass().getSimpleName());
    }

    private void start() {
        System.out.println("Start method called");
    }
}
