package lesson11.cw.rmi;

import lesson11.cw.annotations.Plugin;
import lesson3.bugtracker.Project;

import java.rmi.Remote;

@Plugin(value = "test project")
public interface IProjectService extends Remote {
    Project createProject();
}
