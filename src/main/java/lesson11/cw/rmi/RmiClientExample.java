package lesson11.cw.rmi;

import lesson11.cw.IPlugin;
import lesson3.bugtracker.Project;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class RmiClientExample {
    private static List<Class> plugins = new ArrayList<>();
    private static List<String> pluginConfigs;

    public static void main(String[] args) throws IOException {


        pluginConfigs = Files.lines(Paths.get("/Users/fedkapot/javawebinars/src/main/resources/plugin.properties"), UTF_8).collect(Collectors.toList());

        String pluginConfig = pluginConfigs.get(0).split("=")[1];
        List<String> pluginClassNames = Arrays.asList(pluginConfig.split(","));
        pluginClassNames.forEach(RmiClientExample::addPluginClass);

        String localhost = "127.0.0.1";
        String host = "java.rmi.server.hostname";
        try {
            System.setProperty(host, localhost);


            IProjectService projectService = () -> new Project();

            Class<? extends IProjectService> aClass = projectService.getClass();


            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("projectService", projectService);
            System.out.println("Start " + projectService);
        } catch (RemoteException e) {
            System.err.println("RemoteException : " + e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Exception : " + e.getMessage());
            System.exit(2);
        }
    }

    private static void addPluginClass(String pluginName) {
        try {
            Class pluginClass = Class.forName(pluginName);
            if (isPlugin(pluginClass)) {
                plugins.add(pluginClass);
                Object pluginInstance = pluginClass.getConstructor(null).newInstance(null);
                System.out.printf("Plugin %s instantiated successfully as %s%n", pluginName, pluginInstance.getClass());
                ((IPlugin)pluginInstance).init();
                Method startMethod = pluginInstance.getClass().getDeclaredMethod(pluginConfigs.get(1).split("=")[1], null);
                startMethod.setAccessible(true);
                startMethod.invoke(pluginInstance, null);

                Field passwordField = pluginInstance.getClass().getDeclaredField("password");
                passwordField.setAccessible(true);
                passwordField.set(pluginInstance, "098765");
                passwordField.setAccessible(false);
            } else {
                System.out.println("Class "  + pluginName + " is not a plugin");
            }


        } catch (ClassNotFoundException e) {
            System.out.println("FAILED TO FIND CLASS " + pluginName);
        } catch (NoSuchMethodException e) {
            System.out.println("FAILED TO FIND FIND METHOD IN CLASS " + pluginName);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            System.out.println("FAILED TO INSTANTIATE " + pluginName);
        } catch (NoSuchFieldException e) {
            System.out.println("FAILED TO FIND FIND FIELD IN CLASS " + pluginName);
        }
    }

    private static boolean isPlugin(Class pluginClass) {
        return Arrays.asList(pluginClass.getInterfaces())
                .stream()
                .anyMatch(IPlugin.class::equals);
    }
}
