package lesson12.annotations;

import java.lang.annotation.Annotation;
import java.util.Random;

public class MyAnnotationUsing {
    public static void main(String[] args) {
        Class<?> clazz = getSomeClass();
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof VersionInfo) {
                VersionInfo versionInfo = (VersionInfo) annotation;
                switch (versionInfo.author()) {
                    case "Sergey":
                        System.out.println(clazz.getName() + " good Class");
                        break;
                    case "Artem":
                        System.out.println(clazz.getName() + " bad Class");
                        break;
                    default:
                        System.out.println(clazz.getName() + " know nothing");
                }
            }
        }
    }

    private static Class<?> getSomeClass() {
        return new Random().nextInt(2) == 1 ? AnnotatedClass.class : AnnotatedClass2.class;
    }
}
