package lesson12.io;

import java.io.*;

public class FileUsing {
    public static void main(String[] args) throws IOException {
        File file = new File("test.txt");

        File file2 = new File("src\\main\\resources\\hello.txt");
        System.out.println("file exist - " + file.exists());
        System.out.println("file exist - " + file2.exists());

        PrintStream st = new PrintStream(new FileOutputStream("src\\main\\resources\\output.txt"));
//        System.setErr(st);
        System.setOut(st);

        try (BufferedReader br = new BufferedReader(new FileReader(file2))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        }
        System.out.println("Hello");

    }
}
