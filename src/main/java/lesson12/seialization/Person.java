package lesson12.seialization;

import java.io.Serializable;


public class Person implements Serializable {
    private static final long serialVersionUID = 2L;

    private String name;
    private int age;
    private String surName;
    private String thirdName;

    public Person(String name, int age, String surName, String thirdName) {
        this.name = name;
        this.age = age;
        this.surName = surName;
        this.thirdName = thirdName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", surName='" + surName + '\'' +
                ", thirdName='" + thirdName + '\'' +
                '}';
    }
}
