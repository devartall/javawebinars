package lesson12.seialization;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class PersonExternalizable implements Externalizable {
    private static final long serialVersionUID = 2L;

    private String name;
    private int age;

    @Override
    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.name);
        if (this.name == null) {
            return;
        }
        objectOutput.writeObject(encryptAge(this.age));
    }

    private Object encryptAge(int age) {
        return null;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.name = (String) objectInput.readObject();
        age = decryptAge((Integer) objectInput.readObject());
    }

    private int decryptAge(Integer readObject) {
        return 0;
    }
}
