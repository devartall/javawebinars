package lesson12.seialization;

import java.io.*;

public class SerializeUsing {
    public static final String PATH = "src\\main\\resources\\";

    public static void main(String[] args) {
//        serialize();
        deserialize();


    }



    private static void serialize() {
//        Person person_1 = new Person("Ivan", 25, "Ican", "other");
        PersonExternalizable person1 = new PersonExternalizable();
        //Сериализация
        try (FileOutputStream fos = new FileOutputStream(PATH + "person.ser");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(person1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(person1);
    }

    private static void deserialize() {
        //Десериализация
        Person person_2 = null;
        try (FileInputStream fis = new FileInputStream(PATH + "person.ser");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            person_2 = (Person) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(person_2);
    }
}
