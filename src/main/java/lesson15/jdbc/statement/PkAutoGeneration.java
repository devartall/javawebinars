package lesson15.jdbc.statement;

import java.sql.*;
import java.util.Random;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2020-02-27
 */
public class PkAutoGeneration {

    private static Random random = new Random();

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    private static final String USER = "postgres";
    private static final String PASS = "admin";

    private static final String INSERT_SQL = "INSERT INTO Persons VALUES(nextval('products_pk_custom_id_seq'), 'Иван', 30)";

    public void insertPersons() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
             PreparedStatement stmt = conn.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)) {

            PreparedStatement nextval = conn.prepareStatement("SELECT nextval('hibernate_sequence')");

            long before = System.currentTimeMillis();
            for (int i = 0; i < 10_000; i++) {
                //stmt.executeUpdate();
                nextval.executeQuery();
                //stmt.getGeneratedKeys();
            }
            long after = System.currentTimeMillis();
            System.out.println(after - before);

            System.out.println("Persons inserted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        PkAutoGeneration obj = new PkAutoGeneration();
        obj.insertPersons();
    }
}
