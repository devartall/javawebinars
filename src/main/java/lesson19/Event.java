package lesson19;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
@Scope("prototype")
public class Event {
    private final Calendar date;
    @Value("default message")
    private String message;
    @Value("file")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Event(Calendar date) {
        this.date = date;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Calendar getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Event{" +
                "date=" + date +
                ", message='" + message + '\'' +
                '}';
    }
}
