package lesson19;

import lesson19.configure.AppConfig;
import lesson19.loggers.EventLogger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApp {
    private SpringPerson person;

    public SpringPerson getPerson() {
        return person;
    }

    public SpringApp(SpringPerson person) {
        this.person = person;
    }

    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(AppConfig.class);
        ctx.scan("lesson19");

        ctx.refresh();

        SpringApp app = ctx.getBean("app", SpringApp.class);
        System.out.println(app.getPerson().getFullName());

        Event event1 = ctx.getBean(Event.class);
        Thread.sleep(1000);
        Event event2 = ctx.getBean(Event.class);
        Thread.sleep(1000);
        Event event3 = ctx.getBean(Event.class);

        app.logEvent(event1, "one", ctx);
        app.logEvent(event2, "two", ctx);
        app.logEvent(event3, "three", ctx);
    }

    public void logEvent(Event event, String msg, ApplicationContext ctx) {
        EventLogger logger;

        if (event.getType().equals("info")) logger = (EventLogger) ctx.getBean("eventLogger");
        else logger = (EventLogger) ctx.getBean("fileEventLogger");
        logger.logEvent(event);
    }
}
