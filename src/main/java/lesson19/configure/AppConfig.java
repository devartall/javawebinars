package lesson19.configure;

import lesson19.SpringApp;
import lesson19.SpringPerson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;

@Configuration
public class AppConfig {
    @Bean
    public SpringPerson person() {
        return new SpringPerson("1", "John Smith");
    }

    @Bean
    public SpringApp app(SpringPerson person) {
        return new SpringApp(person);
        //return new SpringApp(person())
    }

    @Bean
    public Calendar date() {
        return Calendar.getInstance();
    }

}
