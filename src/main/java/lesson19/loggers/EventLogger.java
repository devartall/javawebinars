package lesson19.loggers;

import lesson19.Event;

public interface EventLogger {
    void logEvent(Event event);
}
