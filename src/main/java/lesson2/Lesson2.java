package lesson2;


import java.math.BigDecimal;
import java.math.BigInteger;

public class Lesson2 {


    public static void main(String[] args) {
        int integer = 1;
        byte short_type = 124;
        byte byte_type = 42;
        byte result = (byte) (short_type + byte_type);
//        System.out.println(result);

        String str1 = "abc";
        String str2 = "abc";
        String str3 = new String("abc");

        Integer a = 1;
        Integer b = 1;
        Integer c = new Integer(1);

//        System.out.println(a == b);
//        System.out.println(a == c);

//        Object[][] array = new Object[][]{{1, 2}, {"1"}, {4, 5, 6}};
//        for (int i = 0; i < 3; i++) {
//            for (int j = 0; j < 3; j++) {
//                System.out.println(array[i][j]);
//            }
//        }

        double first = 0.1;
        double second = 0.2;
        double third = 0.3;

        System.out.println(first + second);
        System.out.println(third);

        BigInteger bigInteger = new BigInteger("1221341234123412341234");
        BigDecimal bigDecimalFirst = new BigDecimal("0.1");
        BigDecimal bigDecimalSecond = new BigDecimal("0.2");
        BigDecimal bigDecimalThird = new BigDecimal("0.3");

        System.out.println(bigDecimalFirst.add(bigDecimalSecond).equals(bigDecimalThird));

    }
}
