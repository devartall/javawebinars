package lesson20.cw;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@JavaCourseExample
@DependsOn("bookService")
public class AdminService implements ApplicationContextAware {

    private ApplicationContext context;
    BookService bookService = new BookService();

    private BookService autowiredBookService;

    public AdminService() {
        System.out.println("Я родился");
    }

    public AdminService(@Autowired BookService bookService) {
        this.bookService = bookService;
    }

    @Autowired
    public void setAutowiredBookService(BookService autowiredBookService) {
        this.autowiredBookService = autowiredBookService;
        System.out.println("Field injected");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context= applicationContext;
    }

    public void init() {
        BookService bean = context.getBean(BookService.class);
        List<Book> books = bookService.getBooks();
    }
}
