package lesson20.cw;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("bookService-1")
public class BookService implements IService {
    private final List<Book> books = new ArrayList<>();

    public BookService() {
        ServiceLocator.addService("bookService", this);
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books.clear();
        this.books.addAll(books);
    }

    public Optional<Book> getBookByName(String name, User user) {
        if (name == null || "anon".equals(user.getRole())) {
            return Optional.empty();
        }
        return books.stream().filter(book -> name.equals(book.getName())).findAny();
    }
}
