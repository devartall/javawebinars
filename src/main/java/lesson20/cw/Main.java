package lesson20.cw;

import java.util.Base64;

public class Main {

    private static final String JAVA_FOR_DUMMIES = "Java for dummies";

    public static void main(String...args) {

        BookService bookService = new BookService();
        //AdminService adminService = new AdminService();

        Book book = new Book();
        book.setId(1L);
        book.setName(JAVA_FOR_DUMMIES);
        bookService.getBooks().add(book);

        User customer = new User();
        customer.setId(1L);
        customer.setLogin("user");
        customer.setPasswd(Base64.getEncoder().encodeToString("admin".getBytes()));

        bookService.getBookByName(JAVA_FOR_DUMMIES, customer).ifPresent(book1 -> System.out.println("Book " + book1.getName() + " exists"));

        //adminService.init();
    }
}
