package lesson20.cw;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Optional.ofNullable;

public class ServiceLocator {
    private final static Map<String, IService> SERVICES = new ConcurrentHashMap<>();

    public static void addService(String name, IService service) {
        SERVICES.put(name, service);
    }

    public static Optional<IService> getService(String key) {
        return ofNullable(SERVICES.get(key));
    }
}
