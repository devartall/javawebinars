package lesson20.cw;

import java.util.Optional;

public class ThirdPartyService {
    private BookService bookService;

    public ThirdPartyService(BookService bookService) {
        this.bookService = bookService;
    }

    public ThirdPartyService() {
        this.bookService = (BookService)ServiceLocator.getService("bookService").orElse(new BookService());
    }

    public BookService getBookService() {
        return bookService;
    }

    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
}
