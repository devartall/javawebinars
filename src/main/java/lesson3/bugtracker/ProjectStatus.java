package lesson3.bugtracker;

public enum ProjectStatus {
    OPEN, CLOSED, FINISHED
}
