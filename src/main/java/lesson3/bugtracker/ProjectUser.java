package lesson3.bugtracker;

import java.util.Date;
import java.util.Map;

public class ProjectUser extends UserImpl {
    Date registrationDate;
    Map<Long, Role> projectRoles;

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Map<Long, Role> getProjectRoles() {
        return projectRoles;
    }

    public void setProjectRoles(Map<Long, Role> projectRoles) {
        this.projectRoles = projectRoles;
    }

    public ProjectUser(PersonName personName) {
        super(personName);
    }
}
