package lesson3.bugtracker;

/**
 * Абстракция роли
 */
public class Role {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
