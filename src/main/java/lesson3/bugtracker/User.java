package lesson3.bugtracker;

public interface User {
    long getId();

    void setId(long id);

    String getName();

    String getLogin();

    void setLogin(String login);

    String getPassword();

    void setPassword(String password);

    String getEmail();

    void setEmail(String email);

    boolean isActive();

    void setActive(boolean active);
}
