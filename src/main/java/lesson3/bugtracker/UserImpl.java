package lesson3.bugtracker;

public class UserImpl implements User {

    private long id;
    private String login;
    private String password;
    private String email;
    private boolean isActive;

    public static class PersonName {
        private String firstName;
        private String secondName;
        private String lastName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getSecondName() {
            return secondName;
        }

        public void setSecondName(String secondName) {
            this.secondName = secondName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public PersonName(String firstName, String secondName, String lastName) {
            this.firstName = firstName;
            this.secondName = secondName;
            this.lastName = lastName;
        }

        public String getMaskedName() {
            String maskedLastName = lastName.isEmpty() ? lastName : lastName.substring(0, 1);
            return String.format("%s %s %s.", firstName, secondName, maskedLastName);
        }
    }

    private final PersonName personName;

    public UserImpl(PersonName personName) {
        this.personName = personName;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return personName.getMaskedName();
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        isActive = active;
    }
}
