package lesson4.custom;

import lesson4.inner.AnotherUselessClass;
import lesson4.inner.Test;


public class CustomType {
    private static final String TYPE_VALUE = "VALUE";
    Test test;

    // нельзя получить доступ к защищенному типу
    //PackagePrivateType somePrivateValue;

    private SomeInterface implementation;

    public SomeInterface getImplementation() {
        return implementation;
    }

    public void setImplementation(SomeInterface implementation) {
        this.implementation = implementation;
    }

    public CustomType() {
        test = new Test();
        //PackagePrivateType somePrivateValue = test.getSomePrivateValue();

        // анонимный класс
        this.implementation = new SomeInterface() {
            @Override
            public long getValue() {
                SomeInterface someInterface = implementation;
                return 0;
            }
        };
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public static void main(String ... args) {
        CustomType customType = new CustomType();

        // так сделать не получится
        //long importantValue = testExtension.getNestedObject().getImportantValue();
        Object somePrivateValue = customType.getTest().getSomePrivateValue();

        AnotherUselessClass uselessObject = new AnotherUselessClass();
        boolean assignableFrom = Test.class.isAssignableFrom(uselessObject.getClass());
        System.out.println(assignableFrom);
    }

}
