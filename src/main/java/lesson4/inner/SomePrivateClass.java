package lesson4.inner;

public class SomePrivateClass {
    private int someValue;

    public static class NestedInnerClass {
        private int integer;

        public int getInteger() {
            return integer;
        }

        public void setInteger(int integer) {
            this.integer = integer;
        }

        public NestedInnerClass() {
            integer = 2;
        }
    }

    public SomePrivateClass(int someValue) {

        NestedInnerClass nestedInnerObject = new NestedInnerClass();
        nestedInnerObject.integer = 3;
    }

    public int getSomeValue() {
        return someValue;
    }

    public void setSomeValue(int someValue) {
        this.someValue = someValue;
    }

    static void test() {
        SomePrivateClass.NestedInnerClass d;
        d = new SomePrivateClass.NestedInnerClass();
        int integer = d.integer;
    }
}
