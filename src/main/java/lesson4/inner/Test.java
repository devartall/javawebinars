package lesson4.inner;


public class Test {
    protected PackagePrivateType somePrivateValue;
    protected static int testValue;
    String i;

    void uselessMethod() {
        System.out.println("");
    }

    public PackagePrivateType getSomePrivateValue() {
        return somePrivateValue == null ? new PackagePrivateType() : somePrivateValue;
    }

    public void setSomePrivateValue(PackagePrivateType somePrivateValue) {
        this.somePrivateValue = somePrivateValue;
    }

    public static int getTestValue() {
        return testValue;
    }

    public static void setTestValue(int testValue) {
        Test.testValue = testValue;
    }
}
