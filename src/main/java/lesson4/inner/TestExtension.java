package lesson4.inner;

import java.io.Serializable;

public class TestExtension extends Test {
    private static long staticLong = 0L;

    private class NestedClass implements Serializable {
        private long importantValue;

        public NestedClass() {
            PackagePrivateType object = somePrivateValue;
        }

        public long getImportantValue() {
            return importantValue;
        }

        public void setImportantValue(long importantValue) {
            this.importantValue = importantValue;
        }
    }

    private static class StaticClass implements Serializable {
        long importantValue;

        public StaticClass() {
            // так не получится
            //PackagePrivateType object = somePrivateValue;
        }

    }

    private NestedClass nestedObject;
    private StaticClass staticClass;

    public NestedClass getNestedObject() {
        return nestedObject;
    }

    public void setNestedObject(NestedClass nestedObject) {
        this.nestedObject = nestedObject;
    }

    public StaticClass getStaticClass() {
        return staticClass;
    }

    public void setStaticClass(StaticClass staticClass) {
        this.staticClass = staticClass;
    }

        public TestExtension() {

            int testValue = Test.testValue;
            this.nestedObject = new NestedClass();
            long importantValue = nestedObject.importantValue;

            this.staticClass = new StaticClass();
            long importantValue1 = staticClass.importantValue;

            nestedObject.importantValue = 1L;
        }

    private static void test() {
        String value;
    }
}
