package lesson5.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Burkin A. Yur.
 * @ created 2020-06-08
 */
public class Amount {

    private final int sum;
    private List<String> exchanges;

    public Amount(int sum) {
        this.sum = sum;
        this.exchanges = new ArrayList<>();
    }

    public List<String> exchange(int... coins) {
        exchange(this.sum, coins, "");
        List<String> result = new ArrayList<>(exchanges);
        exchanges.clear();

        return result;
    }

    private void exchange(int sum, int[] coins, String s) {
        if (sum > 0) {
            for (int c : coins)
                exchange(sum - c, coins, s + c + " ");
        } else if (sum == 0) {
            exchanges.add(s);
        }
    }

    public static void main(String[] args) {
        Amount amount = new Amount(10);
        List<String> result = amount.exchange(2, 3, 5);

        result.forEach(System.out::println);
    }
}
