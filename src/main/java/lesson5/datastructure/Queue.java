package lesson5.datastructure;

import lesson5.Utils;

/**
 * АСД Queue на базе массива
 *
 * Это учебный пример.
 * Данная реализация небезопасна т.к. возможно переполнение int.
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class Queue {

    public static final int DEFAULT_SIZE = 10;

    private int[] storage;
    private int head;
    private int tail;

    public Queue() {
        storage = new int[DEFAULT_SIZE];
    }

    public Queue(int size) {
        storage = new int[size];
    }

    public void enqueue(int value) {
        if (!isFull())
            storage[tail++ % capacity()] = value;
    }

    public int dequeue() {
        if (isEmpty()) throw new RuntimeException("Queue is empty");

        return storage[head++ % capacity()];
    }

    public int size() {
        return tail - head;
    }

    public int capacity() {
        return storage.length;
    }

    public boolean isEmpty() {
        return head == tail;
    }

    public boolean isFull() {
        return tail - head == storage.length;
    }

    public static void main(String[] args) {
        Queue queue = new Queue(5);

        StringBuilder sb = new StringBuilder();

        sb.append("Enqueue:\n");
        for (int i = 0; i < queue.capacity(); i++) {
            int value = Utils.randomInt(10);
            queue.enqueue(value);
            sb.append(value).append(Utils.COMMA);
        }

        queue.enqueue(-1);

        sb.append("\nDequeue:\n");
        sb.append(queue.dequeue()).append(Utils.COMMA);
        sb.append(queue.dequeue()).append(Utils.COMMA);
        sb.append(queue.dequeue()).append(Utils.COMMA);

        sb.append("\nEnqueue:\n");
        sb.append(100).append(Utils.COMMA);
        sb.append(200).append(Utils.COMMA);
        sb.append(300).append(Utils.COMMA);

        queue.enqueue(100);
        queue.enqueue(200);
        queue.enqueue(300);

        sb.append("\nDequeue:\n");
        for (int i = 0; i < queue.capacity(); i++) {
            sb.append(queue.dequeue()).append(Utils.COMMA);
        }

        System.out.println(sb);
    }
}
