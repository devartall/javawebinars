package lesson5.datastructure;

import lesson5.Utils;

/**
 * АСД Stack на базе массива
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-14
 */
public class Stack {

    public static final int DEFAULT_SIZE = 10;

    private int[] storage;
    private int size;

    public Stack() {
        storage = new int[DEFAULT_SIZE];
    }

    public Stack(int size) {
        storage = new int[size];
    }

    public void push(int value) {
        if (!isFull())
            storage[size++] = value;
    }

    public int pop() {
        if (isEmpty()) throw new RuntimeException("Stack is empty");

        return storage[--size];
    }

    public int size() {
        return size;
    }

    public int capacity() {
        return storage.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == storage.length;
    }

    public static void main(String[] args) {
        Stack stack = new Stack(5);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < stack.capacity(); i++) {
            int value = Utils.randomInt(10);
            stack.push(value);
            sb.append(value).append(Utils.COMMA);
        }

        sb.append("\n");

        stack.push(-1);

        for (int i = 0; i < stack.capacity(); i++) {
            sb.append(stack.pop()).append(Utils.COMMA);
        }

        System.out.println(sb);
    }
}
