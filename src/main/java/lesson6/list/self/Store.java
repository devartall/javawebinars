package lesson6.list.self;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-01-07
 */
public interface Store<T> extends Iterable<T> {

    void add(T value);

    T get(int index);

    int size();

    T remove(int index);
}
