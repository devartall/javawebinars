package lesson6.map.self2;

import java.util.Objects;

/**
 * @author Burkin A. Yur.
 * @ created 2020-06-18
 */
public class Entry<K, V> {

    K key;
    V value;

    Entry(K key) {
        this.key = key;
    }

    Entry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry<?, ?> entry = (Entry<?, ?>) o;

        return Objects.equals(key, entry.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
