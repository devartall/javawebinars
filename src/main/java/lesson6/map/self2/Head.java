package lesson6.map.self2;

/**
 * @author Burkin A. Yur.
 * @ created 2020-06-18
 */
public interface Head<K extends Comparable<K>, V> extends Iterable<Entry<K, V>> {

    boolean add(K key, V value);

    V get(K key);

    V remove(K key);

    int size();
}
