package lesson6.map.self2;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Burkin A. Yur.
 * @ created 2020-06-18
 */
public class LinkedHead<K extends Comparable<K>, V> implements Head<K, V> {

    private LinkedList<Entry<K, V>> list = new LinkedList<>();

    public LinkedHead() {
    }

    public LinkedHead(Head<K, V> head) {
        for (Entry<K, V> entry : head)
            add(entry.key, entry.value);
    }

    @Override
    public boolean add(K key, V value) {
        return list.add(new Entry<>(key, value));
    }

    @Override
    public V get(K key) {
        for (Entry<K, V> entry : list) {
            if (entry.key.equals(key))
                return entry.value;
        }

        return null;
    }

    @Override
    public V remove(K key) {
        V value = get(key);

        if (value != null) {
            list.remove(new Entry<K, V>(key));

            return value;
        }

        return null;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {

            Iterator<Entry<K, V>> iterator = LinkedHead.this.list.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public Entry<K, V> next() {
                return iterator.next();
            }
        };
    }
}
