package lesson6.map.self2;

import java.util.Objects;

/**
 * @author Burkin A. Yur.
 * @ created 2019-01-07
 */
public class SberMap<K extends Comparable<K>, V> {

    private static final int DEFAULT_CAPACITY = 16;
    private static final double DEFAULT_LOAD_FACTOR = 0.75;
    private static final int TREEIFY_THRESHOLD = 8;

    private Head<K, V>[] storage;
    private int size;
    private int threshold;
    private double loadFactor;
    private int loadedCount;

    public SberMap() {
        this.storage = new Head[DEFAULT_CAPACITY];
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        refreshThreshold();
    }

    public SberMap(int capacity, double loadFactor) {
        this.storage = new Head[capacity];
        this.loadFactor = loadFactor;
        refreshThreshold();
    }

    public void put(K key, V value) {
        int index = getIndex(key);

        if (storage[index] == null) {
            storage[index] = new LinkedHead<>();

            loadedCount++;
        } else if (storage[index].size() == TREEIFY_THRESHOLD) {
            storage[index] = new TreeHead<>(storage[index]);
        }

        if (storage[index].add(key, value))
            size++;

        if (loadedCount > threshold)
            resize();
    }

    public V get(K key) {
        int index = getIndex(key);

        return storage[index] != null ? storage[index].get(key) : null;
    }

    public V remove(K key) {
        int index = getIndex(key);

        V value = storage[index] != null ? storage[index].remove(key) : null;

        if ((value != null) && (storage[index].size() == TREEIFY_THRESHOLD))
            storage[index] = new LinkedHead<>(storage[index]);

        return value;
    }

    public int size() {
        return size;
    }

    private int getIndex(K key) {
        Objects.requireNonNull(key, "Key must be not null");

        return (key.hashCode() & 0x7fffffff) % storage.length;
    }

    private void refreshThreshold() {
        threshold = (int)(storage.length * loadFactor);
    }

    private void resize() {
        size = 0;
        loadedCount = 0;

        Head<K, V>[] temp = storage;
        storage = new Head[2 * temp.length];
        refreshThreshold();

        for (Head<K, V> head : temp)
            for (Entry<K, V> entry : head)
                put(entry.key, entry.value);
    }
}
