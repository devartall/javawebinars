package lesson6.map.self2;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Burkin A. Yur.
 * @ created 2020-06-18
 */
public class TreeHead<K extends Comparable<K>, V> implements Head<K, V> {

    private TreeMap<K, V> tree = new TreeMap<>();

    public TreeHead(Head<K, V> head) {
        for (Entry<K, V> entry : head)
            add(entry.key, entry.value);
    }

    @Override
    public boolean add(K key, V value) {
        return tree.put(key, value) != null;
    }

    @Override
    public V get(K key) {
        return tree.get(key);
    }

    @Override
    public V remove(K key) {
        return tree.remove(key);
    }

    @Override
    public int size() {
        return tree.size();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {

            Iterator<Map.Entry<K, V>> iterator = TreeHead.this.tree.entrySet().iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public Entry<K, V> next() {
                Map.Entry<K, V> next = iterator.next();

                return new Entry<>(next.getKey(), next.getValue());
            }
        };
    }
}
