package lesson7;

public class ExceptionHandling {
    public static void main(String[] args) throws Exception {
//        try {
////            throw new Exception("A");
////        } catch (Exception e) {
////            throw new Exception("B");
////        } finally {
////            //throw new Exception("C");
////        }
        System.out.println(getInt());
    }

    public static int getInt() throws Exception {
//        throw new NullPointerException("B");
        try {
            throw new Exception("A");
        } catch (Exception e) {
            throw new Exception("B");
        } finally {
//            throw new Exception("B");
        }

    }
}
