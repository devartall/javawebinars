package lesson7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class LogExample {
    public static final Logger LOGGER = LoggerFactory.getLogger(LogExample.class);

    public static void main(String[] args) {

        for (int i = 0; i < 1000; i++) {

            if (new Random().nextInt(3) == 2) LOGGER.error("Error!!!", new Exception("Bad thing!"));
            LOGGER.trace("Test log record!!!");
            LOGGER.info("Информация");
        }
    }
}
