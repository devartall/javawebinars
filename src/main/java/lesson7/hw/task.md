### Домашнее задание по уроку 7

#### Теория

- [Тест](https://forms.gle/LMcTp64FTz6Why1b6)

#### Практика

Для выполнения практики необходимо доработать существующие классы в пакете [journey](../journey/) 

- Реализуйте интерфейс AutoClosable для Car. Добавьте состояние открытости/закрытости автомобиля
- Метода drive должен выкидывать исключение CloseCarException, если машина пытается поехать в закрытом состоянии.\
CloseCarException должен наследоваться от CarDriveException
- Используйте try with resources в классе CardDriver
- Добавьте наследника JamesBondCar для Car. Переопределите методе drive(), таким образом, чтобы он вызывал super.drive(), а\
также в этом методе исправлялись TireCarException и OverheatEngineException, если температура двигателя двигателя была ниже 150 гардусов.\ 
Температуру необходимо добавить в свойство автомобиля и в свойство исключениия OverheatEngineException
- Замените все System.out.println() на использование LOGGER, с соответствующим уровнем логгирования.
- log4j.properties необходимо настроить так, чтобы события INFO писались **только** в консоль, в ERROR **только** в файл.
- Файл для логов должен существовать в единственном экземпляре и не превышать 5МБ