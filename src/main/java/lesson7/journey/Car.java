package lesson7.journey;

import lesson7.journey.exceptions.CarDriveException;
import lesson7.journey.exceptions.EngineCarException;
import lesson7.journey.exceptions.TireCarException;
import lesson7.journey.exceptions.engine.DestroyedEngineException;
import lesson7.journey.exceptions.engine.OverheatEngineException;

import java.util.Random;

public class Car {
    public void drive() throws CarDriveException {
        if (new Random().nextInt(5) == 0) {
            if (new Random().nextInt(2) == 1) {
                throw new EngineCarException(new OverheatEngineException());
            }
            else {
                throw new EngineCarException(new DestroyedEngineException());
            }
        }
        if (new Random().nextInt(5) == 0) throw new TireCarException();
        System.out.println("Машина едет!");
    }
}
