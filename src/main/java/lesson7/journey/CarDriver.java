package lesson7.journey;

import lesson7.journey.exceptions.CarDriveException;
import lesson7.journey.exceptions.TireCarException;
import lesson7.journey.exceptions.engine.DestroyedEngineException;
import lesson7.journey.exceptions.engine.OverheatEngineException;

public class CarDriver {
    private Car car;

    public CarDriver(Car car) {
        this.car = car;
    }

    public void startDriveTrip() throws CarDriveException, InterruptedException {
        System.out.println("Поездка на машине началась!");
        for (int i = 0; i < 50; i++) {
            try {
                car.drive();
                Thread.sleep(200);
            } catch (TireCarException e) {
                repairTire(e);
            } catch (CarDriveException e) {
                if (e.getCause() instanceof OverheatEngineException) coldEngine((OverheatEngineException)e.getCause());
                if (e.getCause() instanceof DestroyedEngineException) throw e;
            } finally {
                System.out.println("Поездка на машине завершена");
            }
        }
    }

    private void coldEngine(OverheatEngineException e) {
        System.out.println("Охладил двигатель");
    }

    private void repairTire(TireCarException e) {
        System.out.println("Заменили колесо");
    }
}
