package lesson7.journey;

import lesson7.journey.exceptions.CarDriveException;

public class Travel {
    public static void main(String[] args) throws InterruptedException {
        Car car = new Car();
        CarDriver carDriver = new CarDriver(car);

        try {
            System.out.println("Путешествие началось!");
            carDriver.startDriveTrip();
        } catch (CarDriveException e) {
            System.out.println("Поездка завершилась из-за - " + e.getClass());
            System.out.println("Причина - " + e.getCause());
        }

        System.out.println("Путешествие завершено");
    }
}
