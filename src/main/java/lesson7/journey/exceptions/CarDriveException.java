package lesson7.journey.exceptions;

public class CarDriveException extends Exception {
    public CarDriveException(EngineCarException e) {
        super(e);
    }

    public CarDriveException() {
        super();
    }
}
