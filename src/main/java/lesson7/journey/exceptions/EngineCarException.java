package lesson7.journey.exceptions;

public class EngineCarException extends CarDriveException {
    public EngineCarException(EngineCarException e) {
        super(e);
    }

    public EngineCarException() {
        super();
    }
}
