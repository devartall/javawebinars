package lesson7.journey.exceptions.engine;

import lesson7.journey.exceptions.EngineCarException;

public class DestroyedEngineException extends EngineCarException {
    public DestroyedEngineException(EngineCarException e) {
        super(e);
    }

    public DestroyedEngineException() {
        super();
    }
}
