package lesson8.hw;

import lesson8.hw.participants.Student;
import lesson8.hw.participants.Teacher;
import lesson8.hw.space.Room;

import java.util.Set;

public final class ExamInformation {
    private static ExamInformation info;
    private Teacher examiner;
    private Room examRoom;

    private Set<Student> studentGroup;

    public static void setInfo(ExamInformation info) {
        ExamInformation.info = info;
    }

    private ExamInformation() {
    }

    public static ExamInformation getInfo() {
        if (info == null) {
            info = new ExamInformation();
        }
        return info;
    }

    public Teacher getExaminer() {
        return examiner;
    }

    public void setExaminer(Teacher examiner) {
        this.examiner = examiner;
    }

    public Room getExamRoom() {
        return examRoom;
    }

    public void setExamRoom(Room examRoom) {
        this.examRoom = examRoom;
    }

    public int getStudentGroupSize() {
        return studentGroup.size();
    }

    public Set<Student> getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(Set<Student> studentGroup) {
        this.studentGroup = studentGroup;
    }
}
