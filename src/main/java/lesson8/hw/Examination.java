package lesson8.hw;

import lesson8.hw.participants.Student;
import lesson8.hw.participants.Teacher;
import lesson8.hw.space.Room;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Examination {
    private final Set<Student> studentGroup;

    public Examination(Set<Student> studentGroup) {
        this.studentGroup = studentGroup;
    }

    public void startExam() throws InterruptedException {
        initExamInformation();

        for (Student student : studentGroup) {
            new Thread(student).start();
        }

        Thread teacherWork = new Thread(new Teacher());
        teacherWork.start();

        teacherWork.join();
    }

    private void initExamInformation() {
        Teacher examiner = new Teacher();
        Room examRoom = Room.getInstance();

        ExamInformation info = ExamInformation.getInfo();
        info.setExaminer(examiner);
        info.setExamRoom(examRoom);
        info.setStudentGroup(studentGroup);
    }
}
