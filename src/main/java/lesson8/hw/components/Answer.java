package lesson8.hw.components;

/**
 * Ответ на задание
 */
public class Answer {
    private final ExamTask task;
    private final int points;

    public Answer(ExamTask task, int points) {
        this.task = task;
        this.points = points;
    }

    public ExamTask getTask() {
        return task;
    }

    public int getPoints() {
        return points;
    }
}
