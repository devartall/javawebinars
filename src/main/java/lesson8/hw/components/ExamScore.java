package lesson8.hw.components;

/**
 * Оценка
 */
public class ExamScore {
    /**
     * Количество попыток сдачи
     */
    private final int numberOfTry;
    private final TeacherSign sign;

    public ExamScore(int numberOfTry, TeacherSign sign) {
        this.numberOfTry = numberOfTry;
        this.sign = sign;
    }

    public int getNumberOfTry() {
        return numberOfTry;
    }

    public TeacherSign getSign() {
        return sign;
    }
}
