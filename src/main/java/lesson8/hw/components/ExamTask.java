package lesson8.hw.components;

public class ExamTask {
    private final String studentName;

    public ExamTask(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentName() {
        return studentName;
    }
}
