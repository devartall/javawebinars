package lesson8.hw.components;

/**
 * Зачетка
 */
public class ScoreBook {
    private final String studentName;
    private ExamScore score;

    public ScoreBook(String studentName) {
        this.studentName = studentName;
    }

    public ExamScore getScore() {
        return score;
    }

    public void setScore(ExamScore score) {
        this.score = score;
    }

    public String getStudentName() {
        return studentName;
    }
}
