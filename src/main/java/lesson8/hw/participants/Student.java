package lesson8.hw.participants;

import lesson8.hw.ExamInformation;
import lesson8.hw.components.Answer;
import lesson8.hw.components.ExamTask;
import lesson8.hw.components.ScoreBook;
import lesson8.hw.space.*;
import lesson8.hw.utils.ExamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class Student extends Participant implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Student.class);

    private final String name;
    private final ScoreBook scoreBook;

    public Student(String name) {
        this.name = name;
        this.scoreBook = new ScoreBook(this.name);
    }

    public String getName() {
        return name;
    }

    public ScoreBook getScoreBook() {
        return scoreBook;
    }

    @Override
    public void run() {
        boolean isExamContinue = true;
        LOGGER.info("Студент " + name + " пришел на экзамен");
        Room examRoom = ExamInformation.getInfo().getExamRoom();
        Place seat = examRoom.takeFreePlace(this);

        try {
            TeacherPlace teacherPlace = examRoom.getTeacherPlace();
            LOGGER.info("Студент " + name + " ждет экзаминатора");

            TaskBoard taskBoard = teacherPlace.getTaskBoard();
            AnswerBoard answerBoard = teacherPlace.getAnswerBoard();
            int numberOfTries = 0;

            ExamTask task;

            LOGGER.info("Студент " + name + " ждет своего задания");

            task = taskBoard.getTask(name);

            Answer answer = prepareAnswer(task);

            answerBoard.putAnswer(answer);
            LOGGER.info("Студент " + name + " выполнил задание");

            LOGGER.info("Студент " + name + " ждет своей оценки");

            boolean result = teacherPlace.getResult(name);
            teacherPlace.understandResult(name);
            numberOfTries++;

            if (result) {
                isExamContinue = false;
                teacherPlace.forSign(numberOfTries, scoreBook);
                LOGGER.info("Студент " + name + " сдал экзамен и ждет подписи в зачетке");
            }
        } catch (
                Exception e) {
            LOGGER.error("Студент " + name + " вынужден покинуть экзамен", e);
        } finally {
            seat.leavePlace();
            LOGGER.info("Студент " + name + " покинул экзамен");
        }

    }

    private Answer prepareAnswer(ExamTask task) {
        ExamUtils.waitWork(1200);
        return new Answer(task, new Random().nextInt(100));
    }
}
