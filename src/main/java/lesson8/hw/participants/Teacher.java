package lesson8.hw.participants;

import lesson8.hw.ExamInformation;
import lesson8.hw.components.Answer;
import lesson8.hw.components.ExamTask;
import lesson8.hw.components.TeacherSign;
import lesson8.hw.space.*;
import lesson8.hw.utils.ExamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public class Teacher extends Participant implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Teacher.class);

    private TeacherSign sign;

    public Teacher() {
        this.sign = new TeacherSign();
    }

    @Override
    public void run() {
        ExamUtils.waitWork(1000);
        LOGGER.info("Преподаватель начал экзамен");
        Room examRoom = ExamInformation.getInfo().getExamRoom();
        int groupSize = ExamInformation.getInfo().getStudentGroupSize();
        TeacherPlace teacherPlace = examRoom.takeTeacherPlace(this);

        try {
            TaskBoard taskBoard = new TaskBoard();
            AnswerBoard answerBoard = new AnswerBoard();

            teacherPlace.initExamEntities(taskBoard, answerBoard);

            int checkedStudents = 0;

            List<Place> studentPlaces = examRoom.getStudentPlaces();
            LOGGER.info("Преподаватель ждет прихода студентов");

            for (Place studentPlace : studentPlaces) {
                Participant holder = studentPlace.getHolder();
                if (holder instanceof Student) {
                    Student student = (Student) holder;
                    String studentName = student.getName();
                    if (taskBoard.hasTask(studentName)) continue;
                    taskBoard.addTask(studentName, new ExamTask(studentName));
                    LOGGER.info("Преподаватель выдал задание студенту " + studentName);
                }
            }

            Set<Answer> answers = answerBoard.getAnswers();
            for (Answer answer : answers) {
                String studentName = answer.getTask().getStudentName();
                ExamUtils.waitWork(500);
                teacherPlace.addResult(studentName, answer.getPoints() > 50);
                LOGGER.info("Задание студента " + studentName + " проверено " + answers.size());
            }
            answerBoard.clearAnswers();

            int signedExamScore = teacherPlace.signExamScore(sign);
            LOGGER.info("Преподаватель проставил подписи " + signedExamScore + " студентам ");
            checkedStudents += signedExamScore;
            LOGGER.info("Всего проставлено зачетов: " + checkedStudents);
        } catch (Exception e) {
            LOGGER.error("Преподаватель вынужден покинуть экзамен", e);
        } finally {
            teacherPlace.leavePlace();
            LOGGER.info("Экзамен закончен");
        }

    }

    private boolean isStudentPlacesEmpty(List<Place> studentPlaces) {
        for (Place place : studentPlaces) {
            if (!place.isFree()) return false;
        }
        return true;
    }
}
