package lesson8.hw.space;

import lesson8.hw.components.Answer;

import java.util.HashSet;
import java.util.Set;

public class AnswerBoard {
    private final Set<Answer> answerBank;

    public AnswerBoard() {
        this.answerBank = new HashSet<>();
    }

    public Set<Answer> getAnswers() {
        return answerBank;
    }

    public void putAnswer(Answer answer) {
        answerBank.add(answer);
    }

    public void clearAnswers() {
        answerBank.clear();
    }
}
