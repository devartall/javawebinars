package lesson8.hw.space;

import lesson8.hw.participants.Participant;

public class Place {
    private Participant holder;

    public Participant getHolder() {
        return holder;
    }

    void takePlace(Participant holder) {
        this.holder = holder;
    }

    public void leavePlace() {
        holder = null;
    }

    public boolean isFree() {
        return holder == null;
    }
}
