package lesson8.hw.space;

import lesson8.hw.exceptions.AllPlacesOccupiedException;
import lesson8.hw.participants.Student;
import lesson8.hw.participants.Teacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Аудитория для сдачи экзамена
 */
public class Room {
    private static final Logger LOGGER = LoggerFactory.getLogger(Room.class);

    private static Room instance;

    private final int capacity;

    private final List<Place> studentPlaces;
    private final TeacherPlace teacherPlace;

    private Room(int capacity) {
        this.capacity = capacity;

        int studentPlacesCapacity = this.capacity - 1;
        studentPlaces = new ArrayList<>(studentPlacesCapacity);
        for (int i = 0; i < studentPlacesCapacity; i++) {
            studentPlaces.add(new Place());
        }

        teacherPlace = new TeacherPlace();
    }

    public static Room getInstance() {
        if (instance == null) {
            instance = new Room(6);
        }
        return instance;
    }

    public TeacherPlace takeTeacherPlace(Teacher teacher) {
        teacherPlace.takePlace(teacher);
        return teacherPlace;
    }

    public Place takeFreePlace(Student student) {
        for (int i = 0; i < studentPlaces.size(); i++) {
            Place place = studentPlaces.get(i);
            if (place.isFree()) {
                place.takePlace(student);
                LOGGER.info("Студент " + student.getName() + " сел на место " + i);
                return place;
            }
        }

        throw new AllPlacesOccupiedException();
    }

    public List<Place> getStudentPlaces() {
        return studentPlaces;
    }

    public TeacherPlace getTeacherPlace() {
        return teacherPlace;
    }

    public int getCapacity() {
        return capacity;
    }
}
