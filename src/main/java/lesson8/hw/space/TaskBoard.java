package lesson8.hw.space;

import lesson8.hw.components.ExamTask;

import java.util.HashMap;
import java.util.Map;

public class TaskBoard {
    private final Map<String, ExamTask> taskBank;

    public TaskBoard() {
        this.taskBank = new HashMap<>();
    }

    public ExamTask getTask(String studentName) {
        return taskBank.get(studentName);
    }

    public ExamTask addTask(String studentName, ExamTask task) {
        return taskBank.put(studentName, task);
    }

    public boolean hasTask(String studentName) {
        return taskBank.containsKey(studentName);
    }
}