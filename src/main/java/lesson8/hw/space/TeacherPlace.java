package lesson8.hw.space;

import lesson8.hw.components.*;

import java.util.HashMap;
import java.util.Map;

public class TeacherPlace extends Place {
    private TaskBoard taskBoard;
    private AnswerBoard answerBoard;
    private Map<String, Boolean> results;
    private Map<ScoreBook, Integer> forSign;

    public void initExamEntities(TaskBoard taskBoard, AnswerBoard answerBoard) {
        this.taskBoard = taskBoard;
        this.answerBoard = answerBoard;
        this.results = new HashMap<>();
        this.forSign = new HashMap<>();
    }

    public TaskBoard getTaskBoard() {
        return taskBoard;
    }

    public AnswerBoard getAnswerBoard() {
        return answerBoard;
    }

    public void addResult(String studentName, Boolean result) {
        results.put(studentName, result);
    }

    public Boolean getResult(String studentName) {
        return results.get(studentName);
    }

    public void understandResult(String studentName) {
        results.remove(studentName);
    }

    public void forSign(int numberOfTries, ScoreBook scoreBook) {
        forSign.put(scoreBook, numberOfTries);
    }

    public synchronized int signExamScore(TeacherSign sign) {
        int signedScoreBooks = 0;
        for (Map.Entry<ScoreBook, Integer> record : forSign.entrySet()) {
            record.getKey().setScore(new ExamScore(record.getValue(), sign));
            signedScoreBooks++;
        }
        forSign.clear();
        return signedScoreBooks;
    }
}
