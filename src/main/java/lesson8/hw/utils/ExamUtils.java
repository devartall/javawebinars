package lesson8.hw.utils;

public final class ExamUtils {
    private ExamUtils() {
    }

    public static void waitWork(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ignored) {
        }
    }
}
