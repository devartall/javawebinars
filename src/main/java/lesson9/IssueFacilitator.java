package lesson9;

import lesson9.bugtracker.Issue9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Создается задача, проверяется id пользователя, тип задачи, регистрируется событие,
 * событие обрабатывается
 */
public class IssueFacilitator {

    private final static Predicate<String> isNumber = (string) -> isANumber(string);
    private final static List<Long> usersIds = new ArrayList<>();


    public static void main(String[] args) throws IOException {
        usersIds.add(1L);
        usersIds.add(2L);
        Issue9 issue = new Issue9();
        System.out.println("Enter name, userId, priority, status: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        if (isNumber.test(input)) {
            issue.setAuthorId(Long.parseLong(input.split(" ")[1]));
            issue.setName(Optional.ofNullable(input.split(" ")[0])
                                  .orElse(""));
            System.out.println("Issue created: " + issue.getAuthorId());
        }
    }

    private static boolean isANumber(String string) {
        try {
            long id = Long.parseLong(string);
            return usersIds.contains(id);
        }  catch (NumberFormatException e) {
            return false;
        }
    }
}
