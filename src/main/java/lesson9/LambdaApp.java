package lesson9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Тестируем лямбда-выражения в Java
 */
public class LambdaApp {
    private final List<String> items = new ArrayList<>();

    private final IntIsMoreThan10 integerPredicate = new IntIsMoreThan10();
    private final BooleanIsTrue booleanPredicate = new BooleanIsTrue();
    private final LengthEquals4 stringPredicate = new LengthEquals4();

    private final Map<Class<?>, Predicate> predicates = new HashMap<>();

    public static void main(String... args) throws IOException {
        LambdaApp lambdaApp = new LambdaApp();

        //lambdaApp.items.add("1");
        //lambdaApp.items.add("2");
//      //lambdaApp.items.add("11");

        for (int i = 0; i < 100000; i++) {
            lambdaApp.items.add(Integer.toString(i));
        }
        long startTime = new Date().getTime();
        List<String> collect = lambdaApp.items.stream().filter(string -> string.length() >= 2).collect(Collectors.toList());
        long endTime = new Date().getTime() - startTime;

        System.out.println("Process time: " + endTime);

        startTime = new Date().getTime();
        List<String> parallelResult = lambdaApp.items.parallelStream().filter(string -> string.length() >= 2).collect(Collectors.toList());
        endTime = new Date().getTime() - startTime;

        System.out.println("Process time (parallel): " + endTime);

        Stream<String> stringStream = Stream.of("1", "2e", "3", null);
        List<Integer> integers = stringStream.map(LambdaApp::getInteger).collect(Collectors.toList());

        IntStream intStream = IntStream.range(0,10);
        long sum = intStream.map(value -> value + 10).sum();
        long count = IntStream.range(0,10).count();


        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        Object customizedInput = null;

        try {
            Optional<String> input1 = Optional.ofNullable(input);
            customizedInput = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            System.out.println("input is not an integer");
        }

        if (customizedInput == null) {
            customizedInput = input;
        }

        boolean test = lambdaApp.predicates.get(customizedInput.getClass()).test(customizedInput);

        Supplier<String> simpleLogger = new Supplier<String>() {
            @Override
            public String get() {
                return "ok";
            }
        };

        Supplier<String> lambdaLogger = () -> "ok";

        System.out.println(test);
        System.out.println(simpleLogger.get());
    }

    private static Integer getInteger(String s) {
        try {
            return Optional.ofNullable(s).map(Integer::parseInt).orElse(0);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public LambdaApp() {
        predicates.put(Integer.class, (integer) -> (Integer)integer > 10);
        predicates.put(Boolean.class, (variable) -> (Boolean)variable);
        predicates.put(String.class, stringPredicate);

        // функция двух переменных в виде лямбды
        BiFunction<Long, Long, Boolean> biFunction = (x,y) -> getaBoolean(x, y);
        FunctionalInterfaceTest example = () -> System.out.println("this is the test");
        Function<Long, Long> sqrt = (x) -> x*x;

        Runnable task = () -> {
            System.out.println("Started new task at " + new Date().toString());
        };

        Thread t = new Thread(task);
        t.start();
    }


    private Boolean getaBoolean(Long x, Long y) {
        System.out.println("x  = " + x + ", y = " + y);
        return x == y;
    }

    private static class LengthEquals4 implements Predicate<String> {

        @Override
        public boolean test(String s) {

            return s.length() == 4;
        }
    }

    private static class ContainsWord implements Predicate<String> {

        @Override
        public boolean test(String s) {
            return s.contains("test");
        }
    }

    private static class IntIsMoreThan10 implements Predicate<Integer> {

        @Override
        public boolean test(Integer integer) {
            return integer  > 10;
        }
    }

    private static class BooleanIsTrue implements Predicate<Boolean> {

        @Override
        public boolean test(Boolean aBoolean) {
            return aBoolean;
        }
    }

}
