package lesson13.junit;

import lesson13.calculator.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 *
 * @author Burkin A. Yur.
 * @ created 2019-11-26
 */
public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        double result = calculator.add(10, 50);
        assertEquals(60, result, 0);
    }

    @Test
    public void failExample() {
        //Assert.fail();
    }
}
