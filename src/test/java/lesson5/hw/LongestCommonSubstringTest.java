package lesson5.hw;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class LongestCommonSubstringTest {

    @Test
    public void test_solution_1() {
        String result = LongestCommonSubstring.solution_1("", "");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_2() {
        String result = LongestCommonSubstring.solution_1("a", "a");

        assertEquals("a", result);
    }

    @Test
    public void test_solution_3() {
        String result = LongestCommonSubstring.solution_1("a", "b");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_4() {
        String result = LongestCommonSubstring.solution_1("aba", "cfb");

        assertEquals("b", result);
    }

    @Test
    public void test_solution_5() {
        String result = LongestCommonSubstring.solution_1("rjqobjsberlstbk", "jqgqkvpssberkgo");

        assertEquals("sber", result);
    }

    @Test
    public void test_solution_6() {
        String result = LongestCommonSubstring.solution_1("abcd", "efg");

        assertEquals("-1", result);
    }

    @Test
    public void test_solution_7() {
        String result = LongestCommonSubstring.solution_1("sdddfdddd", "kkddddrtddd");

        assertEquals("dddd", result);
    }

    @Test
    public void test_solution_8() {
        String result = LongestCommonSubstring.solution_1("zzxdfg", "uiopzzzzz");

        assertEquals("zz", result);
    }

    @Test
    public void test_solution_9() {
        String result = LongestCommonSubstring.solution_1("uiuiuiuiu", "iuiuiuiuiuiuiuiu");

        assertEquals("uiuiuiuiu", result);
    }

    @Test
    public void test_solution_10() {
        String result = LongestCommonSubstring.solution_1("sberbank", "sberbank");

        assertEquals("sberbank", result);
    }

    @Test
    public void test_solution_11() {
        String result = LongestCommonSubstring.solution_1("sberagilebank", "sbersfbank");

        assertEquals("sber", result);
    }

    @Test
    public void test_solution_12() {
        String result = LongestCommonSubstring.solution_1("abc", "ade");

        assertEquals("a", result);
    }
}
