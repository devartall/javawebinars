package lesson8.hw;

import lesson8.hw.participants.Student;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ExaminationTest {

    @Test
    public void test() throws InterruptedException {
        Set<Student> studentGroup = initStudentGroup();
        Examination examination = new Examination(studentGroup);
        examination.startExam();

        for (Student student : studentGroup) {
            assertNotNull(student.getScoreBook().getScore().getSign());
        }
    }

    private static Set<Student> initStudentGroup() {
        Set<Student> students = new HashSet<>();
        students.add(new Student("One"));
        students.add(new Student("Two"));
        students.add(new Student("Three"));
        students.add(new Student("Four"));
        students.add(new Student("Five"));
        students.add(new Student("Six"));
        students.add(new Student("Seven"));
        students.add(new Student("Eight"));
        students.add(new Student("Nine"));
        students.add(new Student("Ten"));
        students.add(new Student("Eleven"));
        students.add(new Student("Twelve"));
        students.add(new Student("Thirteen"));
        return students;
    }
}
